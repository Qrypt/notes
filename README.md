# Notes

QRYPT: A web-based messaging application that ensures security against the future threats of quantum computers.

The main objective of this project to learn about post crypto graphic techniques and algorithms.

Overview
This project is a web-based messaging app that ensures security against future quantum computing threats by using advanced cryptographic techniques. It allows users to send and receive messages that are encrypted in a way that makes them very hard to intercept or decode by unauthorized parties.

**Key Concepts**
*Quantum-Resistant Cryptography:* A type of encryption designed to be secure even against the powerful computational abilities of future quantum computers.
End-to-End Encryption: Messages are encrypted on the sender's device and only decrypted on the recipient's device. No one else, not even the service provider, can read the messages.
Components
Backend (Server): Handles message encryption and decryption, and provides endpoints for sending and receiving messages.
Frontend (Web Interface): Allows users to interact with the app by sending and receiving messages.

**Example Scenario**

Let's imagine Alice wants to send a secure message to Bob.
Alice and Bob Generate Keys:

When Alice and Bob first use the app, it generates a pair of cryptographic keys for each of them: a public key and a private key.
The public key can be shared with anyone, while the private key is kept secret.
Alice Gets Bob's Public Key:

Alice needs Bob's public key to send him a message. She gets Bob's public key from the server.
Alice Encrypts Her Message:

Alice writes her message, "Hello, Bob!"
Using Bob's public key, the app encrypts the message. This encryption turns the message into a scrambled version that looks like gibberish to anyone who intercepts it.
Sending the Encrypted Message:

The encrypted message is sent to the server and stored temporarily.
Bob Receives the Encrypted Message:

When Bob logs in, he retrieves the encrypted message from the server.
Bob Decrypts the Message:

Using his private key, Bob's app decrypts the message back into its original form, "Hello, Bob!".
Now, Bob can read Alice's message.

**Technical Implementation**
Backend (Server) - Written in Node.js
Generate Keys: When a user first joins, the server generates a public/private key pair.
Store Keys: Public keys are stored on the server so other users can retrieve them.
Encrypt and Decrypt Messages: The server provides endpoints to encrypt messages with a recipient's public key and decrypt messages with the recipient's private key.
Frontend (Web Interface) - Written in React.js
User Interface: Users can input messages, view their public key, and see the encrypted and decrypted messages.
Interactions: The app allows users to fetch public keys, encrypt messages, send them, retrieve encrypted messages, and decrypt them.

**Key Differences**

*Quantum-Resistant Encryption:*
Proposed App: Uses quantum-resistant cryptographic algorithms like Kyber to protect against future quantum computing threats.
WhatsApp: Uses traditional encryption algorithms (e.g., AES-256, RSA) that may become vulnerable to quantum computing attacks in the future.

*Zero-Knowledge Encryption:*
Proposed App: The app provider cannot decrypt or access the messages due to the use of zero-knowledge encryption techniques.
WhatsApp: Although WhatsApp uses end-to-end encryption, it still requires some level of trust in the platform provider (WhatsApp/Facebook) to handle encryption keys securely.

*Self-Destructing Messages:*
Proposed App: Includes an option for messages to automatically delete themselves after being read or after a set period.
WhatsApp: WhatsApp has a similar feature called "disappearing messages," but this is not a core part of its encryption strategy.

*Location-Based Encryption:*
Proposed App: Can restrict decryption of messages to specific geographic locations.
WhatsApp: Does not offer location-based decryption.

*Decentralization:*
Proposed App: Can be designed as a decentralized network, ensuring no central server can be compromised.
WhatsApp: Uses centralized servers owned and controlled by WhatsApp/Facebook.

Example Scenario: Alice and Bob
Alice and Bob Using WhatsApp
Encryption: When Alice sends a message to Bob, WhatsApp uses AES-256 and RSA to encrypt the message. Bob's app decrypts the message using his private key.
Security: Messages are secure from eavesdropping, but WhatsApp/Facebook controls the servers and encryption implementation.
Alice and Bob Using Quantum-Resistant Web Messaging App
Quantum-Resistant Encryption: When Alice sends a message to Bob, the app uses Kyber to encrypt the message, ensuring future quantum computers cannot break the encryption.
Zero-Knowledge Proof: The app provider cannot read Alice's message because it uses zero-knowledge encryption techniques.
Self-Destructing Messages: Alice sets the message to self-destruct after Bob reads it.
Location-Based Decryption: Alice configures the message to only be decrypted if Bob is in a specific location (e.g., his office).
